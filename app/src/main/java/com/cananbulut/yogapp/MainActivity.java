package com.cananbulut.yogapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private LineChart uykuChart,yuruyusChart,nabizChart,nabizOturduguSure;
    private View click_yogaHareketleri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setNoStatusBar();
        click_yogaHareketleri=findViewById(R.id.view_yoga_hareketleri);
        click_yogaHareketleri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),YogaHareketlerActivity.class));
            }
        });

        uykuChart = findViewById(R.id.chart1);
        yuruyusChart = findViewById(R.id.chartYuruyus);
        nabizChart = findViewById(R.id.chartNabiz);
        nabizOturduguSure = findViewById(R.id.chartOturduguSure);
        ChartSetUyku();
        ChartSetYuruyus();
        ChartSetNabiz();
        ChartSetOturduguSure();
    }
    private void ChartSetYuruyus(){
        MyGetChartModel model =new MyGetChartModel();
        model.setChart(yuruyusChart);
        model.setAltBaslik("Yürüyüş Veri Seti");
        model.setDesc("Yürüyüş Süresi");
        model.setAppendix("km");
        model.setmDraw(R.drawable.fade_yellow);
        model.setStartVarb(8);
        model.setEndVarb(10);

        SetVariable(model);
    }
    private void ChartSetUyku(){
        MyGetChartModel model =new MyGetChartModel();
        model.setChart(uykuChart);
        model.setAltBaslik("Uyku Veri Seti");
        model.setDesc("Uyku Süresi");
        model.setAppendix("%");
        model.setmDraw(R.drawable.fade_red);
        model.setStartVarb(3);
        model.setEndVarb(5);
        model.setMaxDeger(65);
        model.setKatsayi(1);

        SetVariable(model);
    }
    private void ChartSetNabiz(){
        MyGetChartModel model =new MyGetChartModel();
        model.setChart(nabizChart);
        model.setAltBaslik("Nabız Veri Seti");
        model.setDesc("Nabız Oranı");
        model.setAppendix("bpm");
        model.setmDraw(R.drawable.fade_blue);
        model.setStartVarb(8);
        model.setEndVarb(10);

        SetVariable(model);
    }

    private void ChartSetOturduguSure(){
        MyGetChartModel model =new MyGetChartModel();
        model.setChart(nabizOturduguSure);
        model.setAltBaslik("Oturma Süresi Veri Seti");
        model.setDesc("Oturma Süresi Oranı");
        model.setAppendix("dk");
        model.setmDraw(R.drawable.fade_green);
        model.setStartVarb(8);
        model.setEndVarb(10);

        SetVariable(model);
    }


    private void setLineChartValue(LineChart mChart){
        List<ILineDataSet> sets = mChart.getData()
                .getDataSets();

        for (ILineDataSet iSet : sets) {

            LineDataSet set = (LineDataSet) iSet;
            set.setMode(set.getMode() == LineDataSet.Mode.CUBIC_BEZIER
                    ? LineDataSet.Mode.CUBIC_BEZIER
                    :  LineDataSet.Mode.CUBIC_BEZIER);
        }
        mChart.invalidate();

    }
    private void SetVariable(MyGetChartModel model){
        final LineChart mChart=model.getChart();
        ArrayList<Entry> values = new ArrayList<>();

        int katsayi,maxDeger;

        if (model.getMaxDeger()!=0){
            maxDeger=model.getMaxDeger();
        }else {
            maxDeger=28;
        }

        if (model.getKatsayi()!=0){
            katsayi=model.getKatsayi();
        }else {
            katsayi=3;
        }

        for (int i = 1; i < model.getEndVarb(); i++) {

            int atanansayi=new Random().nextInt(maxDeger)*katsayi;
            if (atanansayi==0){
                atanansayi= katsayi;
            }
            values.add(new Entry(i+i, atanansayi));
        }
        Description desc=new Description();
        desc.setText(model.getDesc());
        mChart.setDescription(desc);
        LineDataSet set1;
        set1 = new LineDataSet(values,model.getAltBaslik() );

        set1.setDrawIcons(false);

        // draw dashed line
        //set1.enableDashedLine(3f, 1f, 0f);

        // black lines and points
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.TRANSPARENT);

        // line thickness and point size
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);

        // draw points as solid circles
        set1.setDrawCircleHole(false);

        // customize legend entry
        set1.setFormLineWidth(1f);
        //set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        //set1.setFormSize(10.f);

        // text size of values
        set1.setValueTextSize(9f);

        // draw selection line as dashed
        set1.enableDashedHighlightLine(10f, 5f, 0f);

        set1.setDrawFilled(true);
        set1.setFillFormatter(new IFillFormatter() {
            @Override
            public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                return mChart.getAxisLeft().getAxisMinimum();
            }
        });


        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter(" "+model.getAppendix()));

        leftAxis.setTypeface(Typeface.DEFAULT);
        //leftAxis.setAxisMinimum(1f);
        //leftAxis.setAxisMaximum(100f);

        mChart.getAxisRight().setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setLabelRotationAngle(80);
        xAxis.setValueFormatter(new LargeValueFormatter("/"+"03/2019"));
        xAxis.setTypeface(Typeface.DEFAULT);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //xAxis.setAxisMinimum(1f);
        //xAxis.setAxisMaximum(count);

        Drawable drawable = ContextCompat.getDrawable(this,model.getmDraw());
        set1.setFillDrawable(drawable);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1); // add the data sets

        // create a data object with the data sets
        LineData data = new LineData(dataSets);

        // set data
        mChart.setData(data);

        setLineChartValue(mChart);

    }
    public void setNoStatusBar(){
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    private class MyGetChartModel{
        private LineChart chart;
        private String desc;
        private String appendix;
        private String altBaslik;
        private int mDraw;
        private int startVarb;
        private int endVarb;
        private int maxDeger;
        private int katsayi;

        public int getMaxDeger() {
            return maxDeger;
        }

        public void setMaxDeger(int maxDeger) {
            this.maxDeger = maxDeger;
        }

        public int getKatsayi() {
            return katsayi;
        }

        public void setKatsayi(int katsayi) {
            this.katsayi = katsayi;
        }

        public String getAppendix() {
            return appendix;
        }

        public void setAppendix(String appendix) {
            this.appendix = appendix;
        }

        public LineChart getChart() {
            return chart;
        }

        public void setChart(LineChart chart) {
            this.chart = chart;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getAltBaslik() {
            return altBaslik;
        }

        public void setAltBaslik(String altBaslik) {
            this.altBaslik = altBaslik;
        }

        public int getmDraw() {
            return mDraw;
        }

        public void setmDraw(int mDraw) {
            this.mDraw = mDraw;
        }

        public int getStartVarb() {
            return startVarb;
        }

        public void setStartVarb(int startVarb) {
            this.startVarb = startVarb;
        }

        public int getEndVarb() {
            return endVarb;
        }

        public void setEndVarb(int endVarb) {
            this.endVarb = endVarb;
        }
    }
}
