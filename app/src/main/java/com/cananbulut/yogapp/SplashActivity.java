package com.cananbulut.yogapp;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setNoStatusBar();

        Thread timerThread= new Thread(){
            public void run(){
                try {
                    sleep(2100);
                }catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        };
        timerThread.start();
        setANimation();
    }
    public void setNoStatusBar(){
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void setANimation(){
        ImageView imageView2 =findViewById(R.id.imageView3);
        imageView2.setBackgroundResource(R.drawable.animation2);
        AnimationDrawable empaAnimation2=(AnimationDrawable) imageView2.getBackground();
        empaAnimation2.start();

    }
}
