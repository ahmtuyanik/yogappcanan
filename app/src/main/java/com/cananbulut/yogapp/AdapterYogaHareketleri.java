package com.cananbulut.yogapp;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ahmetuyanik on 25.05.2019.
 */

public class AdapterYogaHareketleri  extends RecyclerView.Adapter<AdapterYogaHareketleri.viewHolder>{

    private List<YogaHareketlerActivity.YogaResimModel> list;
    private Context context;
    public AdapterYogaHareketleri(List<YogaHareketlerActivity.YogaResimModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_holder_yoga_hareketleri, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder h, int i) {
        YogaHareketlerActivity.YogaResimModel model=list.get(i);

        h.iv_hareket.setImageDrawable(context.getResources().getDrawable(list.get(i).getImage()));

        if (model.getTitle()!=null){
            h.tv_baslik.setText((model.getTitle()));
        }
        if (model.getStatik()!=null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                h.tv_aciklama.setText(Html.fromHtml(model.getStatik(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                h.tv_aciklama.setText(Html.fromHtml(model.getStatik()));
            }
        }else {
            h.tv_aciklama.setText("");
        }
    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder{
        ImageView iv_hareket;
        TextView tv_baslik,tv_aciklama;
        public viewHolder(@NonNull View v) {
            super(v);
            iv_hareket=v.findViewById(R.id.iv_hareket_resmi);
            tv_baslik=v.findViewById(R.id.textView3);
            tv_aciklama=v.findViewById(R.id.textView5);
        }
    }
}
