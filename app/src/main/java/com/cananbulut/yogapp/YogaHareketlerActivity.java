package com.cananbulut.yogapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class YogaHareketlerActivity extends AppCompatActivity {


    private RecyclerView rc_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yoga_hareketler);
        rc_list=findViewById(R.id.rc_list);
        setAdapterList();

    }
    private void setAdapterList(){
        List<YogaResimModel> list=new ArrayList<>();

        YogaResimModel
        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_yeni_1);
        model.setTitle(yoga_yeni_baslik());
        model.setUrl(null);
        model.setStatik(yoga_string_1());
        list.add(model);

        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_yeni_2);
        model.setTitle(yoga_yeni_baslik());
        model.setUrl(null);
        model.setStatik(yoga_string_2());
        list.add(model);

        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_yeni_3);
        model.setTitle(yoga_yeni_baslik());
        model.setUrl(null);
        model.setStatik(yoga_string_3());
        list.add(model);

        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_yeni_4);
        model.setTitle(yoga_yeni_baslik());
        model.setUrl(null);
        model.setStatik(yoga_string_4());
        list.add(model);


        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_1);
        model.setTitle("Çocuk Pozu");
        model.setStatik(yoga_yeni_baslik());
        model.setUrl(null);
        list.add(model);


        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_2);
        model.setTitle("Köprü");
        model.setStatik(yoga_yeni_baslik());
        model.setUrl(null);
        list.add(model);


        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_3);
        model.setStatik(yoga_yeni_baslik());
        model.setTitle("Öne Eğilme");
        model.setUrl(null);
        list.add(model);


        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_4);
        model.setStatik(yoga_yeni_baslik());
        model.setTitle("Kobra Pozu");
        model.setUrl(null);
        list.add(model);


        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_5);
        model.setTitle("Sandalye Pozu");
        model.setStatik(yoga_yeni_baslik());
        model.setUrl(null);
        list.add(model);


        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_6);
        model.setStatik(yoga_yeni_baslik());
        model.setTitle("Savaşçı Pozu");
        model.setUrl(null);
        list.add(model);


        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_7);
        model.setStatik(yoga_yeni_baslik());
        model.setTitle("Karın Aşağı Pozu");
        model.setUrl(null);
        list.add(model);


        model =new YogaResimModel();
        model.setImage(R.drawable.yoga_8);
        model.setTitle("Çekirge Pozu");
        model.setUrl(null);
        list.add(model);


        AdapterYogaHareketleri adapter=new AdapterYogaHareketleri(list,getApplicationContext());
        rc_list.setAdapter(adapter);


    }

    private String yoga_yeni_baslik(){
        return "Günlük yoga hareketlerinizi tamamladiginizda;\n" +
                "\n" +
                "Uyku verimliliğiniz % 40 artacaktir.\n" +
                "Stres orani %60 düşecektir.\n" +
                "Beden yorgunluğu %35 azalacaktır.\n" +
                "Enerji seviyeniz:%43 artacaktır.";
    }

    private String yoga_string_1(){
         return "<h2><p><span style=\"font-size: 24pt; color: #ff0000;\">SHALABASANA</span></p>\n" +
               "<p><span style=\"font-size: 16.5pt;\">(1 DAKİKA)</span></p></h2>\n" +
               "                                    <p></p><p>Yüzükoyun uzanın. Kollar kalçanın iki yanında ve avuç içleri yere dönük olsun. Nefes alırken önce baş, boyun ve omuzlar yerden sıyrılmaya başlasın, nefes verirken burada kalın. Bir sonraki nefes alışınızda kolları ve bacakları da kaldırın ve elleri arkada birleştirip, kolları topuklara doğru iyice uzatırken kalça kaslarınızı sıkarak bacaklarınızı ayak parmaklarınıza kadar gerin. Boynunuz uzun, alın yere paralel olsun. Pozda sadece kaburga, alt karın ve leğen kemiklerinizin üzerinde kaldığınıza emin olun. Bu şekilde 4-5 nefes kadar kalın ve nefes verirken kendinizi yavaşça yere doğru bırakın. Başı bir tarafa çevirip kalçayı sağa sola sallayarak pozun etkilerini rahatlatın. 3 tur uygulayın.</p>\n" +
               "<p><em><strong>FAYDALARI: Bozuk bir postür ve sırt ağrılarına karşı her gün uygulayabilirsiniz. Omurga, kalça, arka kol ve bacak kaslarını güçlendirir. Omuz, göğüs, bel ve üst bacak kaslarını esnetir. Duruşunuzu düzeltir. Karın bölgesindeki organları harekete geçirir. Stresi atmaya ve sırt ağrılarını hafifletmeye yardımcı olur.</strong></em></p><p></p>\t\t\t\t\t                                  ";
    }

    private String yoga_string_2(){
        return "                                   <h2><p><span style=\"font-size: 24pt; color: #ff0000;\">UTTANASANA</span></p>\n" +
                "<p><span style=\"font-size: 16.5pt;\">(1 DAKİKA)</span></p></h2>\n" +
                "                                    <p></p><p>Ayaklar kalça mesafesinde açık olsun, nefes verirken kalçadan öne doğru uzun bir omurgayla katlanın. Eğer gövdeniz izin veriyorsa düz bacaklarla veya bükük dizlerle, elleri ayak bileklerinin arkasından tutarak yerleştirin. Bir başka seçenek de elleri veya el parmak uçlarınızı ayakların hemen önüne yerleştirmek olabilir. Boyun ve baş rahatça aşağı sarksın, omuzlarınız kulaklardan uzak olsun. Gövde yuvarlanabilir ama kamburlaşmamaya özen gösterin. Yer çekiminin yardımı ile her nefes verişte biraz daha katlanabilmeyi araştırın. Baş dönmesi, göz kararması yaşamamak için duruştan yavaş bir şekilde çıkın.</p>\n" +
                "<p><em>İPUCU:&nbsp;Uygulamanız boyunca uzun ve derin nefesler alıp verin.</em></p>\n" +
                "<p><strong>FAYDALARI: Sırt kaslarını, baldırları, dış arkası kirişlerini ve kalçayı esnetir, rahatlatır. Yorgunluk ve kaygı hislerini hafifletir. Baş ağrıları, uykusuzluk, stres ve depresyona iyi gelir.</strong></p><p></p>\t\t\t\t\t                                  ";
    }

    private String yoga_string_3(){
        return "                               <h2><p><span style=\"font-size: 24pt; color: #ff0000;\">PRASARİTA PADOTTANASANA</span></p>\n" +
                "<p><span style=\"font-size: 16.5pt;\">(1 DAKİKA)</span></p></h2>\n" +
                "                                    <p></p><p>Ayaklarınızın dış kenarları matın kısa kenarlarına paralel olacak şekilde ayakta durun. İki ayağınız arasında bir bacak uzunluğu kadar mesafe olsun. Göğüs kafesi açık, omurga uzun… Ellerinizi belinize koyup, nefes verirken kalçalardan öne katlanmaya başlayın ve ellerinizi omuz hizasında yere koyun, avuçlarınız tamamen yere değsin. Omuzlar kulaklardan uzaklaşırken, kürek kemikleri geride birbirine yaklaşsın. Başınızı da yere değdirerek destek alın. Dilerseniz başınızın altına bir yoga bloğu koyarak da uygulayabilirsiniz. Yoga uygulamanız ilerledikçe, bu pozda başın tepesi yere kadar inebilir. Nefes alıp vererek pozda bekleyin. Ellerinizi belinize koyup yavaşça doğrularak başlangıç pozisyonuna dönün.</p>\n" +
                "<p><strong>FAYDALARI: Omurgayı, iç ve arka bacakları esnetip güçlendirir. Vücuttaki enerji akışını sağlayarak baş ağrılarının hafiflemesine, uykudan önce zihni sakinleştirmeye yardımcı olur.</strong></p><p></p>\t\t\t\t\t                                  ";
    }

    private String yoga_string_4(){
        return "                              <h2><p><span style=\"font-size: 24pt; color: #ff0000;\">JANU SİRSANA</span></p>\n" +
                "<p><span style=\"font-size: 16.5pt;\">(HER İKİ BACAKLA 1’ER DAKİKA)</span></p></h2>\n" +
                "                                    <p></p><p>Yere oturun ve bacaklarınızı ileri doğru uzatın. Bir bacağınızı bükün ve ayak tabanınızı iç bacağın baldırına mümkün olduğunca kasığa yakın olacak şekilde dayayın. Ardından önde uzun olan bacağın üzerine ileri doğru uzanarak katlanın. Bedeninizin izin verdiği ölçüde öne doğru uzanarak katlandıktan sonra, pozda bedeni gevşeterek doğal nefeslerle kalın. Eller yakalayabiliyorsa ayağı tutabilir. Zaman içerisinde beden açıldıkça baş dize yaklaşabilir. Rahatça öne doğru katlanamıyorsanız kalçanın altına bir destek (battaniye, minder gibi) kullanmak yardımcı olabilir. Nefes alarak doğrulun ve vücudunuzun diğer tarafıyla da tekrarlayın.</p>\n" +
                "<p><strong>FAYDALARI: Omurga, omuz, diz arkasındaki kirişleri, kasıkları esnetir. Beyni sakinleştirir, sinir, halsizlik, kaygı, sinüzit, baş ağrısı, adet rahatsızlıklarını azaltır. Sırt ağrılarını giderir, omurgayı kuvvetlendirir. Uykusuzluğa iyi gelir.</strong></p><p></p>\t";
    }
    public class YogaResimModel{
        private String title;
        private String url;
        private int image;
        private String statik;

        public String getStatik() {
            return statik;
        }

        public void setStatik(String statik) {
            this.statik = statik;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getImage() {
            return image;
        }

        public void setImage(int image) {
            this.image = image;
        }
    }
}
